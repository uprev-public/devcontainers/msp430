# toolchain cmake file for msp430-gcc

# Example CMakelists.txt for msp430-gcc 
#
#   cmake_minimum_required(VERSION 2.8)
#   set(CMAKE_TOOLCHAIN_FILE ${MSP430_CMAKE_TOOLCHAIN_FILE})
#
#   project(TEST)
#
#   set(MSP430_MCU msp430g2553)
#   set(MSP430_MCU_FREQ 16000000UL)
#
#   msp430_add_executable(test main.c)





# needs the following variables:
# MSP430_MCU : mcu type 
# MSP430_MCU_FREQ : clock frequency (defines F_CPU)

# generic avr flags
set(MSP430_CFLAGS "-ffunction-sections -fdata-sections -I/opt/ti/msp430-gcc/include" CACHE STRING "MSP430 compilation flags")
set(MSP430_LFLAGS "-Wl,--relax,--gc-sections -L/opt/ti/msp430-gcc/include" CACHE STRING "MSP430 link flags")

# find toolchain programs
set(MSP430-GCC /opt/ti/msp430-gcc/bin/msp430-elf-gcc)
set(MSP430-GXX /opt/ti/msp430-gcc/bin/msp430-elf-g++)
set(MSP430-OBJCOPY /opt/ti/msp430-gcc/bin/msp430-elf-objcopy)
set(MSP430-SIZE /opt/ti/msp430-gcc/bin/msp430-elf-size)
set(MSP430-OBJDUMP /opt/ti/msp430-gcc/bin/msp430-elf-objdump)
set(MSPDEBUG /opt/ti/msp430-gcc/bin/msp430-elf-gdb)

# define toolchain
set(CMAKE_SYSTEM_NAME Generic)

# set C and C++ compilers
set(CMAKE_C_COMPILER ${MSP430-GCC} CACHE FILEPATH "MSP430 C Compiler")
set(CMAKE_CXX_COMPILER ${MSP430-GXX} CACHE FILEPATH "MSP430 C++ Compiler")

# Release by default
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
endif()

function(msp430_add_executable_compilation EXECUTABLE)
  
  set(EXECUTABLE_ELF "${EXECUTABLE}.elf")

  # main target for the executable depends of elf
  add_custom_target(${EXECUTABLE} ALL DEPENDS ${EXECUTABLE_ELF})
  

  # compile and link elf file
  add_executable(${EXECUTABLE_ELF} ${ARGN})
  set_target_properties(${EXECUTABLE_ELF} PROPERTIES 
    COMPILE_FLAGS "-mmcu=${MSP430_MCU} -DF_CPU=${MSP430_MCU_FREQ} ${MSP430_CFLAGS}"
    LINK_FLAGS "-mmcu=${MSP430_MCU} ${MSP430_LFLAGS}")

  # display size info after compilation
  add_custom_command(TARGET ${EXECUTABLE} POST_BUILD
    COMMAND ${MSP430-SIZE} ${EXECUTABLE_ELF})
endfunction(msp430_add_executable_compilation)

function(msp430_add_executable_upload EXECUTABLE)
  add_custom_target(upload_${EXECUTABLE} 
    COMMAND ${MSPDEBUG} -q rf2500 "prog ${EXECUTABLE}.elf"
    DEPENDS ${EXECUTABLE})
endfunction(msp430_add_executable_upload)

function(msp430_add_executable EXECUTABLE)
  if(NOT MSP430_MCU)
    message(FATAL_ERROR "MSP430_MCU not defined")
  endif()
  msp430_add_executable_compilation(${EXECUTABLE} ${ARGN})
  msp430_add_executable_upload(${EXECUTABLE})
endfunction(msp430_add_executable)
