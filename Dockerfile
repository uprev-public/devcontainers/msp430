FROM uprev/base:ubuntu-18.04

# Install base packages
RUN apt-get update &&  apt-get install -y \
        apt-utils \
        zlib1g-dev \
        ca-certificates \
        apt-transport-https \
        gnupg software-properties-common \
        libusb-0.1-4 \
        libgconf-2-4 \
        gdb

# Install MSP430-GCC and support files
ENV MSP430_GCC_VERSION 9.3.1.11
RUN mkdir -p /opt/ti
ADD msp430-gcc-${MSP430_GCC_VERSION}_linux64.tar.bz2 /tmp/
ADD msp430-gcc-support-files-1.212.zip /tmp/
WORKDIR /tmp
RUN     tar xf msp430-gcc-${MSP430_GCC_VERSION}_linux64.tar.bz2 && \
        unzip msp430-gcc-support-files-1.212.zip && \
        mv msp430-gcc-${MSP430_GCC_VERSION}_linux64 /opt/ti/msp430-gcc && \
        mkdir -p /opt/ti/msp430-gcc/include && \
        mv msp430-gcc-support-files/include/* /opt/ti/msp430-gcc/include/ && \
        rm msp430-gcc-${MSP430_GCC_VERSION}_linux64.tar.bz2 && \
        rm msp430-gcc-support-files-1.212.zip && \
        rm -rf msp430-gcc-support-files
ENV PATH /opt/ti/msp430-gcc/bin:$PATH
ENV MSP430_TOOLCHAIN_PATH /opt/ti/msp430-gcc

# Install UniFlash
ENV UNIFLASH_VERSION=5.2.0.2519
RUN wget "http://software-dl.ti.com/ccs/esd/uniflash/uniflash_sl.${UNIFLASH_VERSION}.run" && \
        chmod +x uniflash_sl.${UNIFLASH_VERSION}.run && \
        ./uniflash_sl.${UNIFLASH_VERSION}.run --unattendedmodeui none --mode unattended --prefix /opt/ti/uniflash && \
        rm uniflash_sl.${UNIFLASH_VERSION}.run && \
        cd /opt/ti/uniflash/TICloudAgentHostApp/install_scripts && \
        mkdir -p /etc/udev/rules.d && \
        cp 70-mm-no-ti-emulators.rules /etc/udev/rules.d/72-mm-no-ti-emulators.rules && \
        cp 71-ti-permissions.rules /etc/udev/rules.d/73-ti-permissions.rules && \
        ln -sf /lib/x86_64-linux-gnu/libudev.so.1 /lib/x86_64-linux-gnu/libudev.so.0


# Copy in toolchain file 
ADD msp430-gcc-toolchain.cmake /opt/ti/msp430-gcc/msp430-gcc-toolchain.cmake
ENV MSP430_CMAKE_TOOLCHAIN_FILE /opt/ti/msp430-gcc/msp430-gcc-toolchain.cmake

WORKDIR /
